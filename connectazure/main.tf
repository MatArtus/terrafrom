provider "azurerm"{
  subscription_id	="3f963f42-f471-4496-a658-d4810e94df5b"
  client_id		="7820de2e-39f3-4a4e-b7c1-f34092ed5f9d"
  client_secret		="89e98b88-c6f6-4d90-9dd8-94de5d39b8dc"
  tenant_id 		="403ad817-c577-4501-a104-fb0aad645ad6"
}

resource "azurerm_resource_group" "myterraformgroup" {
	name = "Gerard"
	location = "eastus"
	tags {
	 environment =  "Terraform Demo"
}
}
resource "azurerm_virtual_network" "myterraformnetwork" {
   name			= "myVnet"
   address_space	= ["10.0.0.0/16"]
   location		= "eastus"
   resource_group_name	= "${azurerm_resource_group.myterraformgroup.name}"
   tags {
	environment = "Terraform Demo"
}

}

resource "azurerm_subnet" "myterraformsubnet" {
name 		= "mySubnet"
resource_group_name ="${azurerm_resource_group.myterraformgroup.name}"
virtual_network_name ="${azurerm_virtual_network.myterraformnetwork.name}"
address_prefix = "10.0.2.0/24"
}

resource "azurerm_public_ip" "myterraformpublicip"{
  name 			="myPublicIP"
  location		="eastus"
  resource_group_name 	="${azurerm_resource_group.myterraformgroup.name}"
  public_ip_address_allocation= "dynamic"
  tags {
       environment = "Terraform Demo"
} 
}

resource "azurerm_network_security_group" "myterraformnsg"{
  name		= "myNetworkSecurityGroup"
  location	= "eastus"
  resource_group_name= "${azurerm_resource_group.myterraformgroup.name}"
  tags {
    enironment = "Terraform Demo"
	}
  security_rule {
    name	="SSH"
    priority	=1001
    direction	="Inbound"
    access	="Allow"
    protocol	="Tcp"
    source_port_range ="*"
    destination_port_range ="22"
    source_address_prefix  ="*"
    destination_address_prefix="*" 
	}
}

resource "azurerm_network_interface""myterraformnic"{
  name  ="myNIC"
  location ="eastus"
  resource_group_name= "${azurerm_resource_group.myterraformgroup.name}"

  ip_configuration {
    name	="myNicConfiguration"
    subnet_id 	="${azurerm_subnet.myterraformsubnet.id}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id	="${azurerm_public_ip.myterraformpublicip.id}"
}
}

resource "random_id" "randomId" {
keepers = {# Generate a new ID only when a new resource group is defined
resource_group = "${azurerm_resource_group.myterraformgroup.name}"
}
byte_length = 8
}

resource "azurerm_storage_account" "mystorageaccount" {
name = "diag${random_id.randomId.hex}"
resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
location = "eastus"
account_replication_type = "LRS"
account_tier = "Standard"
tags {
environment = "Terraform Demo"
}
}

resource "azurerm_virtual_machine" "myterraformvm" {
name = "myVM"
location = "eastus"
resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
network_interface_ids = ["${azurerm_network_interface.myterraformnic.id}"]
vm_size = "Standard_DS1_v2"

storage_os_disk {
name = "myOsDisk"
caching = "ReadWrite"
create_option = "FromImage"
managed_disk_type = "Premium_LRS"
}
storage_image_reference {
publisher = "Canonical"
offer = "UbuntuServer"
sku = "16.04.0-LTS"
version = "latest"
}

os_profile {
computer_name = "myvm"
admin_username = "Mathieu"
}
os_profile_linux_config {
disable_password_authentication = true
ssh_keys {
path = "/home/Mathieu/.ssh/authorized_keys"
key_data = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDjEKfahaVW19un2aYikdwxY70XsHkosnPFv7RsQm2qUdjiF8WiWCnvfKqQNMHhe7ZRPKETacnp+XJHCrgTB4pdDUYBk9GJhPy+aA7/9Zs52LQRSwhoaH5egI7mtZiogTGD8A/LpWdrXCUxMM0eyBoDRvf9bUWDEOHCLiaz56qVEZR2+Mz+IMXts5ts/AQxO6zkeJvB8YXXyXAShv2RpyLUL4hLQCavPyGx8me9+viK0N4tYyAGLJKWgtCX8LQfv/2in+HhIgzRr3g0yAtlqIYVGNnc1fi4zJ8yARWV9nrlikbZmQh3QES1XmguFPAUjfMxiTI6vjWM0bwFPTHTlzF1 maVM"
}
}
boot_diagnostics {
enabled = "true"
storage_uri = "${azurerm_storage_account.mystorageaccount.primary_blob_endpoint}"
}
tags {
environment = "Terraform Demo"
}
}

