variable "subscription_id"{}
variable  "client_id"{}
variable "client_secret"{}
variable "tenant_id"{}
variable  "resource_group"{}
variable "virtual_network" {}
variable "subnet" {}
variable "adresse" {type = "list"}
variable "NIC" {type = "list"}
variable "Ip_config" {type = "list"}
variable "Security" {type = "list"}
variable "Storage" {}
variable "VM_name" {type = "list"}
variable "disk" {type = "list"}
variable "computer_name" {type = "list"}
variable "location" {}
variable "admin_name" {}
variable "admin_passwd" {}
variable "ssh_pub" {}