variable "subscription_id"{}
variable  "client_id"{}
variable "client_secret"{}
variable "tenant_id"{}
variable  "resource_group"{}
variable "virtual_network" {}
variable "subnet" {}
variable "adresse" {type = "list"}
variable "NIC" {}
variable "Ip_config" {}
variable "Security" {}
variable "Storage" {}
variable "VM_name" {}
variable "disk" {}
variable "computer_name" {}
variable "location" {}
variable "admin_name" {}
variable "admin_passwd" {}
variable "ssh_pub" {}