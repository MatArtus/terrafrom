
resource "azurerm_resource_group" "newresourcegroup" {
  name     = "${var.resource_group}"
  location = "${var.location}"
}
resource "azurerm_virtual_network" "newvirtualnetwork" {
  name                = "${var.virtual_network}"
  resource_group_name = "${azurerm_resource_group.newresourcegroup.name}"
  address_space       = ["10.0.0.0/16"]
  location            = "${var.location}"
}
resource "azurerm_subnet" "newsubnet" {
  name                 = "${var.subnet}"
  resource_group_name  = "${azurerm_resource_group.newresourcegroup.name}"
  virtual_network_name = "${azurerm_virtual_network.newvirtualnetwork.name}"
  address_prefix       = "10.0.1.0/24"
}
resource "azurerm_public_ip" "newpublicip" {
  count                        = "${length(var.adresse)}"
  name                         = "Pub_ip${count.index+1}"
  location                     = "${var.location}"
  resource_group_name          = "${azurerm_resource_group.newresourcegroup.name}"
  public_ip_address_allocation = "dynamic"
  #ip_address                   = "${element(var.adresse, count.index)}"
}
resource "azurerm_network_interface" "newnetworkinterface" {
  count               = "${length(var.adresse)}"
  name                = "${var.NIC}${count.index+1}"
  location            = "${azurerm_resource_group.newresourcegroup.location}"
  resource_group_name = "${azurerm_resource_group.newresourcegroup.name}"

  ip_configuration {
    name                          = "${var.Ip_config}${count.index}"
    subnet_id                     = "${azurerm_subnet.newsubnet.id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "${var.adresse [count.index]}"
    public_ip_address_id          = "${element(azurerm_public_ip.newpublicip.*.id, count.index)}"
  }
}

resource "azurerm_network_security_group" "newnetworksecuritygroup" {
  name                = "${var.Security}"
  location            = "${azurerm_resource_group.newresourcegroup.location}"
  resource_group_name = "${azurerm_resource_group.newresourcegroup.name}"
  security_rule {
    name                       = "SSH1"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "SSH2"
    priority                   = 1002
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}


resource "random_id" "randomId" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group = "${azurerm_resource_group.newresourcegroup.name}"
    }

    byte_length = 8
}
resource "azurerm_storage_account" "newstorageaccountgtm" {
  name                     = "${var.Storage}"
  resource_group_name      = "${azurerm_resource_group.newresourcegroup.name}"
  location                 = "${var.location}"
  account_replication_type = "LRS"
  account_tier = "Standard"
}
resource "azurerm_virtual_machine" "newVM1" {
    count                 ="${length(var.adresse)}"
    name                  = "${var.VM_name}${count.index+1}"
    location              = "${var.location}"
    resource_group_name   = "${azurerm_resource_group.newresourcegroup.name}"
    network_interface_ids = ["${element(azurerm_network_interface.newnetworkinterface.*.id, count.index+1)}"]
    vm_size               = "Standard_DS1_V2"



    storage_os_disk {
        name              = "${var.disk}${count.index+1}"
        caching           = "ReadWrite"
        create_option     = "FromImage"
        managed_disk_type = "Premium_LRS"
    }
storage_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "16.04.0-LTS"
        version   = "latest"
    }
    os_profile {
        computer_name  = "${var.computer_name}${count.index+1}"
        admin_username = "stage"
    }
os_profile_linux_config {
        disable_password_authentication = true
        ssh_keys {
            path     = "/home/stage/.ssh/authorized_keys"
            key_data = "${var.ssh_pub}"
        }
    }
    boot_diagnostics {
        enabled     = "true"
        storage_uri = "${azurerm_storage_account.newstorageaccountgtm.primary_blob_endpoint}"
    }
}
